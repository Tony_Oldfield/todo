﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using TODO.Models;

namespace TODO.Controllers
{
    [Authorize]
    [RoutePrefix("api/TODO")]
    public class TodoController : ApiController
    {
        private TODOContext db = new TODOContext();

        // GET: api/Todo
        [Route("")]
        public IQueryable<TodoItem> GetTodoItems()
        {
            var name = ClaimsPrincipal.Current.Identity.Name;
            return db.TodoItems.Where(a => a.Owner == name).OrderByDescending(a => a.Id);
        }

        [Route("{filter}")]
        public IQueryable<TodoItem> GetTodoItems(string filter)
        {
            var name = ClaimsPrincipal.Current.Identity.Name;
            bool completed;
            if (filter == "Completed")
                completed = true;
            else if (filter == "Uncompleted")
                completed = false;
            else
                return GetTodoItems();

            return db.TodoItems.Where(a => a.Owner == name && a.Completed == completed).OrderByDescending(a => a.Id);
        }

        // GET: api/Todo/5
        [ResponseType(typeof(TodoItem))]
        [Route("{id:int}", Name ="GetTodoById")]
        public IHttpActionResult GetTodoItem(int id)
        {
            TodoItem todoItem = db.TodoItems.Find(id);
            var name = ClaimsPrincipal.Current.Identity.Name;
            if (todoItem == null || todoItem.Owner != name)
            {
                return NotFound();
            }

            return Ok(todoItem);
        }

        // PUT: api/Todo/5
        [ResponseType(typeof(void))]
        [Route("{id:int}")]
        public IHttpActionResult PutTodoItem(int id, TodoItem todoItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todoItem.Id)
            {
                return BadRequest();
            }

            var name = ClaimsPrincipal.Current.Identity.Name;
            TodoItem currentItem = db.TodoItems.Find(id);
            if (currentItem == null || name != currentItem.Owner)
            {
                return BadRequest();
            }

            //allow partial updates (user does not need to supply all values, ommitted values retain current item value)
            if (todoItem.Title != null)
                currentItem.Title = todoItem.Title;
            if (todoItem.Description != null)
                currentItem.Description = todoItem.Description;
            currentItem.Completed = todoItem.Completed;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Todo
        [ResponseType(typeof(TodoItem))]
        [Route("")]
        public IHttpActionResult PostTodoItem(TodoItem todoItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var name = ClaimsPrincipal.Current.Identity.Name;
            todoItem.Owner = name;

            db.TodoItems.Add(todoItem);
            db.SaveChanges();

            return CreatedAtRoute("GetTodoById", new { id = todoItem.Id }, todoItem);
        }

        // DELETE: api/Todo/5
        [ResponseType(typeof(TodoItem))]
        [Route("{id:int}")]
        public IHttpActionResult DeleteTodoItem(int id)
        {
            TodoItem todoItem = db.TodoItems.Find(id);
            var name = ClaimsPrincipal.Current.Identity.Name;
            if (todoItem == null || todoItem.Owner != name)
            {
                return NotFound();
            }

            db.TodoItems.Remove(todoItem);
            db.SaveChanges();

            return Ok(todoItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TodoItemExists(int id)
        {
            return db.TodoItems.Count(e => e.Id == id) > 0;
        }
    }
}