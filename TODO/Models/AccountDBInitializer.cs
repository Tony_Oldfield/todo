﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace TODO.Models
{
    public class AccountDBInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser { Email = "test1@email.com", UserName = "admin@myemail.com" };
            Task<IdentityResult> createTask = manager.CreateAsync(user, "Password1!");
            createTask.Wait();
            base.Seed(context);
        }
    }
}