﻿var app = angular.module('myApp', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
    .when('/', { redirectTo: 'list' })
    .when('/login', {
        templateUrl: 'pages/login.html',
        controller: 'LoginController'
    })
    .when('/add', {
        templateUrl: 'pages/add.html',
        controller: 'AddController'
    })
    .when('/register', {
        templateUrl: 'pages/register.html',
        controller: 'RegisterController'
    })
    .when('/list/:filter?', {
        templateUrl: 'pages/list.html',
        controller: 'ListController'
    })
    .otherwise({ redirectTo: '/' });
});

app.controller('RegisterController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    $scope.register = function () {
        var data = {
            Email: $scope.username,
            Password: $scope.password,
            ConfirmPassword: $scope.password
        };

        $http({
            url: 'http://localhost:49994/api/Account/Register',
            method: 'POST',
            data: data,
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }
        })
        .success(function (data, status, headers, config) {
            $location.path('login');
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
    };
}]);

app.controller('LoginController', ['$scope', '$http', '$httpParamSerializerJQLike', '$window', '$location', function ($scope, $http, $httpParamSerializerJQLike, $window, $location) {
    $scope.login = function () {
        var data = {
            grant_type: "password",
            username: $scope.username,
            password: $scope.password
        };

        $http({
            url: 'http://localhost:49994/Token',
            method: 'POST',
            data: $httpParamSerializerJQLike(data),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .success(function (data, status, headers, config) {
            $window.sessionStorage.setItem("oauth", data.access_token);
            $location.path('/');
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
    };
}]);

app.controller('ListController', ['$scope', '$http', '$window', '$location', '$routeParams', function ($scope, $http, $window, $location, $routeParams) {

    var token = $window.sessionStorage.getItem("oauth");
    if (token == null) {
        $location.path('login');
        return;
    }

    $scope.markAsComplete = function (todo) {
        var data = {
            Id: todo.Id,
            Completed: "true"
        };
        $http({
            url: 'http://localhost:49994/api/TODO/' + todo.Id,
            method: 'PUT',
            data: data,
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
        .success(function (data, status, headers, config) {
            todo.Completed = true;
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
    };

    $scope.delete = function (todo) {
        $http({
            url: 'http://localhost:49994/api/TODO/' + todo.Id,
            method: 'DELETE',
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
        .success(function (data, status, headers, config) {
            todo.Deleted = true;
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
    };

    var url = 'http://localhost:49994/api/TODO';
    if ($routeParams.filter != null)
        url = url + "/" + $routeParams.filter;
    $scope.params = $routeParams;

    $http({
        url: url,
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        }
    })
        .success(function (data, status, headers, config) {
            for (var i = 0; i < data.length; i++)
                data[i].Deleted = false;
            $scope.todos = data;
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
}]);

app.controller('AddController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
    
    var token = $window.sessionStorage.getItem("oauth");
    if (token == null) {
        $location.path('login');
        return;
    }

    $scope.addTodo = function () {
        var data = {
            Title: $scope.title,
            Description: $scope.description
        };

        $http({
            url: 'http://localhost:49994/api/TODO',
            method: 'POST',
            data: data,
            headers: {
                'Authorization': 'Bearer ' + token
            }
        })
        .success(function (data, status, headers, config) {
            $location.path('/list/Uncompleted');
        })
        .error(function (data, status, headers, config) {
            alert("error");
        });
    };


}]);